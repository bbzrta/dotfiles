#!/usr/bin/env bash 

lxsession &
picom &
/usr/bin/emacs --daemon &
volumeicon &
nm-applet &

### UNCOMMENT ONLY ONE OF THE FOLLOWING THREE OPTIONS! ###
# 1. Uncomment to restore last saved wallpaper
xargs xwallpaper --stretch < ~/Pictures/wallpapers/3840x2160.png &
# 2. Uncomment to set a random wallpaper on login
 find ~/Pictures/wallpapers/ -type f | shuf -n 1 | xargs xwallpaper --stretch &
# 3. Uncomment to set wallpaper with nitrogen
 nitrogen --restore &
