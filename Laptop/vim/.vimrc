syntax on

set noerrorbells
set tabstop=4 
set softtabstop=4
set shiftwidth=4
set smartindent
set nu
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set encoding=utf-8


filetype indent on
highlight ColorColumn ctermbg=0 guibg=lightgrey

call plug#begin('~/.vim/plugged')
	Plug 'git@github.com:joshdick/onedark.vim.git', { 'as': 'onedark' }
	Plug 'dracula/vim', { 'as': 'dracula' }
	Plug 'preservim/nerdtree'
	Plug 'git@github.com:Valloric/YouCompleteMe.git'
call plug#end()


nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :terminal<CR>
nnoremap <C-s> :source %<CR>
